<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_subscribe}}`.
 */
class m190630_180528_create_user_subscribe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_subscribe}}', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'date' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_subscribe}}');
    }
}
