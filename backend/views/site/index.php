<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
        </tr>
        </thead>
        <tbody>
        <? $i = 0;
        foreach ($users as $user) {?>
        <tr>
            <th scope="row"><?=$i++?></th>
            <td><?=$user->info->name?></td>
            <td>Otto</td>
            <td>@mdo</td>
        </tr>
        <? } ?>
        </tbody>
    </table>

</div>
